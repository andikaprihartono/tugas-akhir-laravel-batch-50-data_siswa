@extends('layouts.master')

@section('title')
Edit Data Guru
    
@endsection


{{-- @push('script')
    Membuat Tools Tabel
@endpush --}}


@section('content')
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="/dataguru/{{$guru->id}}">
    @csrf
    @method('put')
        <div class="mb-3">
            <label  class="form-label">Nama Guru</label>
            <input type="text" class="form-control" name="nama" value="{{$guru->nama}}">
        </div>
        {{-- <div class="mb-3">
            <label  class="form-label">Jabatan</label>
            <input type="text" class="form-control" name="umur">
        </div> --}}
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
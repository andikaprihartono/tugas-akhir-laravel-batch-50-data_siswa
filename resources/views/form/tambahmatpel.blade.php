@extends('layouts.master')

@section('title')
Form Data Mata Pelajaran
    
@endsection

@section('content')
        
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form method="POST" action="/datamatpel">
    @csrf
    <div class="mb-3">
        <label  class="form-label">Nama Mata Pelajaran</label>
        <input type="text" class="form-control" name="namamatpel">
    </div>
    <div class="mb-3">
        <label  class="form-label">Nama Guru</label>
        <select name="guru_id" id="guru_id" class="form-control">
            <option value="">--Pilih--</option>
            @foreach ($guru as $guru)
                <option value="{{$guru->id}}">{{$guru->nama}}</option>
            @endforeach
        </select>
        {{-- <input type="text" class="form-control" name="nama"> --}}
    </div>
    {{-- <div class="mb-3">
        <label  class="form-label">Jabatan</label>
        <input type="text" class="form-control" name="umur">
    </div> --}}
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection

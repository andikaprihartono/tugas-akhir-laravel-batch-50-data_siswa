@extends('layouts.master')

@section('title')
Data Guru
    
@endsection


{{-- @push('script')
    Membuat Tools Tabel
@endpush --}}


@section('content')
<a href="/guru/create" class="btn btn-primary my-3">Tambah Data</a>
<h4 class="card-title">Table Data Guru</h4>
 
                    <div class="table-responsive">
                      <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($guru as $key => $item)
                            <tr>
                                <th scope="row">{{$key+1}}</th>
                                <td>{{$item->nama_guru}}</td>
                                <td>
                                    <form action="/guru/{{$item->id}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <a href="/guru/{{$item->id}}/edit" class="btb btn-secondary btn-sm">Edit</a>
                                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                    </form>
                                </td>
                            </tr>
                             @empty
                            <tr>
                                <td>Tidak ada Guru</td>
                            </tr>
                            @endforelse 
                        </tbody>
                      </table>
                    </div>
    
@endsection

@extends('layouts.master')

@section('title')
Data Siswa
@endsection


{{-- @push('script')
    Membuat Tools Tabel
@endpush --}}


@section('content')
<a href="siswa/create" class="btn btn-primary my-3">Tambah Data</a>
<h4 class="card-title">Table Data Siswa</h4>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>NIS</th>
                    <th>Nama</th>
                    <th>Tempat Lahir</th>
                    <th>Tanggal Lahir</th>
                    <th>Kelas</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($siswa as $sw=>$value)
                    <tr>
                        <td>{{$sw + 1}}</th>
                        <td>{{$value->nis}}</td>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->tempat_lahir}}</td>
                        <td>{{$value->tanggal_lahir}}</td>
                        <td>{{$value->kelas_id}}</td>
                        <td>
                            <a href="/siswa/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/siswa/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse
            </tbody>
        </table>              
    
@endsection
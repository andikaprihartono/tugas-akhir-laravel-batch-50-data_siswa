@extends('layouts.master')

@section('title')
Ubah Data Siswa
@endsection
<div>
    <h2>Edit Siswa {{$siswa->id}}</h2>
    <form action="/siswa/{{$siswa->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">NIS</label>
            <input type="text" class="form-control" name="nis" value="{{$siswa->nis}}" id="nis" placeholder="Masukkan NIS">
            @error('title')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Nama</label>
            <input type="text" class="form-control" name="nama"  value="{{$siswa->nama}}"  id="nama" placeholder="Masukkan Nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Tempat Lahir</label>
            <input type="text" class="form-control" name="tempat_lahir"  value="{{$siswa->tempat_lahir}}"  id="nama" placeholder="Masukkan Tempat Lahir">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="body">Tanggal Lahir</label>
            <input type="date" class="form-control" name="nama"  value="{{$siswa->tanggal_lahir}}"  id="nama" placeholder="Masukkan Tanggal Lahir">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@section('content')
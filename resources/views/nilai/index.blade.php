@extends('layouts.master')

@section('title')
Data Nilai
    
@endsection


{{-- @push('script')
    Membuat Tools Tabel
@endpush --}}


@section('content')
<a href="/nilai/create" class="btn btn-primary my-3">Tambah Data</a>
<h4 class="card-title">Table Data Nilai</h4>
 
                    <div class="table-responsive">
                      <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Siswa</th>
                                <th>Mata Pelajaran</th>
                                <th>Nilai</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($nilai as $key => $item)
                            <tr>
                                <th scope="row">{{$key+1}}</th>
                                <td>{{$item->nama_siswa}}</td>
                                <td>{{$item->nama_matpel}}</td>
                                <td>{{$item->nilai}}</td>
                                <td>
                                    <form action="/nilai/{{$item->id}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <a href="/nilai/{{$item->id}}/edit" class="btb btn-secondary btn-sm">Edit</a>
                                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                    </form>
                                </td>
                            </tr>
                             @empty
                            <tr>
                                <td>Tidak ada Nilai</td>
                            </tr>
                            @endforelse 
                        </tbody>
                      </table>
                    </div>
    
@endsection

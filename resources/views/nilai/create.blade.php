@extends('layouts.master')

@section('title')
Data Nilai
    
@endsection


{{-- @push('script')
    Membuat Tools Tabel
@endpush --}}


@section('content')
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="/nilai">
    @csrf
        <div class="mb-3">
            <label  class="form-label">Nama Siswa</label>
            <input type="text" class="form-control" name="nama_siswa">
        </div>
        
        <div class="mb-3">
            <label  class="form-label">Mata Pelajaran</label>
            <input type="text" class="form-control" name="mapel">
        </div>
        
        <div class="mb-3">
            <label  class="form-label">Nilai</label>
            <input type="text" class="form-control" name="nilai">
        </div>
        
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
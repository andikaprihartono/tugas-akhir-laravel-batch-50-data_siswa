@extends('layouts.master')

@section('title')
Edit Data Nilai
    
@endsection


{{-- @push('script')
    Membuat Tools Tabel
@endpush --}}


@section('content')
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="/nilai/{{$nilai->id}}">
    @csrf
    @method('put')
        <div class="mb-3">
            <label  class="form-label">Nama Siswa</label>
            <input type="text" class="form-control" name="nama_siswa" value="{{$nilai->nama_siswa}}">
        </div>
        
        <div class="mb-3">
            <label  class="form-label">Mata Pelajaran</label>
            <input type="text" class="form-control" name="mapel" value="{{$nilai->mapel}}">
        </div>
        
        <div class="mb-3">
            <label  class="form-label">Nilai</label>
            <input type="text" class="form-control" name="nilai" value="{{$nilai->nilai}}">
        </div>
        
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
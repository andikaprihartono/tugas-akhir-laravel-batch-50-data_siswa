@extends('layouts.master')

@section('title')
Ubah Data Kelas
@endsection
<div>
    <h2>Edit Kelas {{$kelas->id}}</h2>
    <form action="/kelas/{{$kelas->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="kelas">Kelas</label>
            <input type="text" class="form-control" name="kelas" value="{{$kelas->nis}}" id="kelas" placeholder="Masukkan Kelas">
            @error('kelas')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
    <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>
@section('content')
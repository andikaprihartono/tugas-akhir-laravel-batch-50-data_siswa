@extends('layouts.master')

@section('title')
Data Kelas
@endsection

{{-- @push('script')
    Membuat Tools Tabel
@endpush --}}

@section('content')
<a href="kelas/create" class="btn btn-primary my-3">Tambah Data</a>
<h4 class="card-title">Table Data Kelas</h4>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Kelas</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($kelas as $kl=>$value)
                    <tr>
                        <td>{{$kl + 1}}</th>
                        <td>{{$value->kelas}}</td>
                        <td>
                                    <form action="/kelas/{{$item->id}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <a href="/kelas/{{$item->id}}/edit" class="btb btn-secondary btn-sm">Edit</a>
                                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                    </form>
                                </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse
            </tbody>
        </table>              
  
@endsection
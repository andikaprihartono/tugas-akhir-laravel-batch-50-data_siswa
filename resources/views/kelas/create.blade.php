@extends('layouts.master')

@section('title')
Form Tambah Kelas
@endsection

@section('content')
<form action="/kelas" method="POST">
    @csrf
    <div class="form-group">
        <label for="kelas">Kelas</label>
        <input type="text" class="form-control" name="kelas" id="kelas" placeholder="Masukkan Kelas">
        @error('title')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>  
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection
@extends('layouts.master')

@section('title')
Data Mata Pelajaran
    
@endsection


{{-- @push('script')
    Membuat Tools Tabel
@endpush --}}


@section('content')
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="/matpel">
        @csrf
            <div class="mb-3">
                <label  class="form-label">Mata Pelajaran</label>
                <input type="text" class="form-control" name="nama_matpel">
            </div>
            {{-- <div class="mb-3">
                <label  class="form-label">Jabatan</label>
                <input type="text" class="form-control" name="umur">
            </div> --}}
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
@endsection
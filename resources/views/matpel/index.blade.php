@extends('layouts.master')

@section('title')
Data Mata Pelajaran
    
@endsection


{{-- @push('script')
    Membuat Tools Tabel
@endpush --}}


@section('content')
<a href="/matpel/create" class="btn btn-primary my-3">Tambah Data</a>
<h4 class="card-title">Table Data Mata Pelajaran</h4>
 
                    <div class="table-responsive">
                      <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Mata Pelajaran</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse ($matpel as $key => $item)
                            <tr>
                                <th scope="row">{{$key+1}}</th>
                                <td>{{$item->nama_matpel}}</td>
                                <td>
                                    <form action="/matpel/{{$item->id}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <a href="/matpel/{{$item->id}}/edit" class="btb btn-secondary btn-sm">Edit</a>
                                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                    </form>
                                </td>
                            </tr>
                             @empty
                            <tr>
                                <td>Tidak ada Mata Pelajaran</td>
                            </tr>
                            @endforelse 
                        </tbody>
                      </table>
                    </div>
    
    
@endsection

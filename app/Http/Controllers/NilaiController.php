<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class NilaiController extends Controller
{
    public function index(){
        $nilai = DB::table('nilai')->get();
        return view('nilai.index', ['nilai'=>$nilai]);
    }
    public function create(){
        return view('nilai.create');
    }
    public function save(Request $request)
    {
        $request->validate([
            'siswa_id' => 'required',
            'kelas_id' => 'required',
            'nilai' => 'required',
        ]);
        DB::table('nilai')->insert([
            'siswa_id' => $request->input('siswa_id'),
            'kelas_id' => $request->input('kelas_id'),
            'nilai' => $request->input('nilai'),
        ]);

        return redirect('/nilai');
    }
    public function edit($id)
    {
        $nilai= DB::table('nilai')->find($id);
        return view('nilai.edit', ['nilai'=>$nilai]);
    }
    
    public function update($id, Request $request)
    {
        $request->validate([
            'siswa_id' => 'required',
            'kelas_id' => 'required',
            'nilai' => 'required',
        ]);

        $affected = DB::table('nilai')
            ->where('id', $id)
            ->update(
                [
                    'siswa_id' => $request->input('siswa_id'),
                    'kelas_id' => $request->input('kelas_id'),
                    'nilai' => $request->input('nilai'),
                ]
            );
            return redirect('/nilai');
    }
    public function destroy($id)
    {
        DB::table('nilai')->where('id', $id)->delete();
        return redirect('/nilai');
    }
}

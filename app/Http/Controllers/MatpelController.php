<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;  

use App\Models\Guru;
use App\Models\Matpel;
use Illuminate\Http\Request;

class MatpelController extends Controller
{
    public function index(){
        $matpel = DB::table('matpel')->get();
        // return view('cast.indexcast', ['cast'=>$cast]);
        return view('matpel.index', ['matpel'=>$matpel]);
    }
    public function create(){
        return view('matpel.create');
    }
    public function save(Request $request)
    {
        $request->validate([
            'nama_matpel' => 'required|max:255',
        ]);
        DB::table('matpel')->insert([
            'nama_matpel' => $request->input('nama_matpel'),
        ]);

        return redirect('/matpel');
    }
    public function edit($id)
    {
        $matpel= DB::table('matpel')->find($id);
        return view('matpel.edit', ['matpel'=>$matpel]);
    }
    
    public function update($id, Request $request)
    {
        $request->validate([
            'nama_matpel' => 'required|max:255',
        ]);

        $affected = DB::table('matpel')
              ->where('id', $id)
              ->update(
                [
                    'nama_matpel' => $request->input('nama_matpel'),
                ]
            );
            return redirect('/matpel');
    }
    public function destroy($id)
    {
        DB::table('matpel')->where('id', $id)->delete();
        return redirect('/matpel');
    }
}

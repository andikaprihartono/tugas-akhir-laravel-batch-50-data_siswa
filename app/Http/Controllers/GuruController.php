<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class GuruController extends Controller
{
    public function index(){
        $guru = DB::table('guru')->get();
        // return view('cast.indexcast', ['cast'=>$cast]);
        return view('guru.index', ['guru'=>$guru]);
    }
    public function create(){
        return view('guru.create');
    }
    public function save(Request $request)
    {
        $request->validate([
            'nama_guru' => 'required|max:255',
        ]);
        DB::table('guru')->insert([
            'nama_guru' => $request->input('nama_guru'),
        ]);

        return redirect('/guru');
    }
    public function edit($id)
    {
        $guru= DB::table('guru')->find($id);
        return view('guru.edit', ['guru'=>$guru]);
    }
    
    public function update($id, Request $request)
    {
        $request->validate([
            'nama_guru' => 'required|max:255',
        ]);

        $affected = DB::table('guru')
              ->where('id', $id)
              ->update(
                [
                    'nama_guru' => $request->input('nama_guru'),
                ]
            );
            return redirect('/guru');
    }
    public function destroy($id)
    {
        DB::table('guru')->where('id', $id)->delete();
        return redirect('/guru');
    }
}

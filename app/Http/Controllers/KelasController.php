<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KelasController extends Controller
{
    
    public function index()
    {
        $kelas = DB::table('kelas')->get();
        return view('kelas.index',['kelas'=>$kelas]);
    }

    public function create(){
        return view('kelas.create');
    }

    public function save(Request $request)
    {
        $this->validate($request,[
            'nama_kelas' => 'required',
        ]);
        DB::table('kelas')->insert([
            'nama_kelas' => $request->input('nama_kelas'),
        ]);
        return redirect('/kelas');
    }

    public function edit($id)
    {
        $kelas = DB::table('kelas')->find($id);
        return view('kelas.edit', ['kelas'=>$kelas]);
    }

    public function update($id,Request $request)
    {
        $request->validate([
            'nama_kelas' => 'required',
        ]);
        $affected = DB::table('kelas')
            ->where('id',$id)
            ->update(
                [
                    'nama_kelas' => $request->input('nama_kelas'),
                ]
                );
        return redirect('/kelas');
    }

    public function destroy($id)
    {
        DB::table('kelas')->where('id',$id)->delete();
        return redirect('/kelas');
    }
}

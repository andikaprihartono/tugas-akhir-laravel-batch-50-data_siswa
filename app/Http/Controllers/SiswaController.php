<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Models\Siswa;

class SiswaController extends Controller
{
    public function index()
    {
        $siswa = DB::table('siswa')->get();
        return view('siswa.index', ['siswa'=>$siswa]);
    }

    public function create(){
        return view('siswa.create');
    }

    public function save(Request $request){
        $this->validate($request,[
            'nis' => 'required',
            'nama_siswa' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
        ]);

       DB::table('siswa')->insert([
            'nis' => $request->input('nis'),
            'nama_siswa' => $request->input('nama_siswa'),
            'tempat_lahir' => $request->input('tempat_lahir'),
            'tanggal_lahir' => $request->input('tanggal_lahir'),
        ]);
        return redirect('/siswa');
    }

    public function edit($id)
    {
        $siswa = DB::table('siswa')->find($id);
        return view('siswa.edit', ['siswa'=>$siswa]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'nis' => 'requiered',
            'nama_siswa' => 'required',
            'tempat_lahir' => 'required',
            'tanggal_lahir' => 'required',
        ]);

        $affected = DB::table('siswa')
            ->where('id',$id)
            ->update(
                [
                    'nis'=> $request->input('nis'),
                    'nama_siswa'=> $request->input('nama_siswa'),
                    'tempat_lahir'=> $request->input('tempat_lahir'),
                    'tanggal_lahir'=> $request->input('tanggal_lahir'),
                ]
            );
        return redirect('/siswa');
    }

    public function destroy($id)
    {
        DB::table('siswa')->where('id',$id)->delete();
        return redirect('/siswa');
    }

}

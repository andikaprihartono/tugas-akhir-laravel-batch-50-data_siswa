<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Matpel_Kelas extends Model
{
    protected $table = ['matpel_id','guru_id','kelas_id'];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    protected $table = "siswa";
    protected $fillable = ["nis","nama_siswa","tempat_lahir","tanggal_lahir"];



    public function nilai()
    {
    return $this->hasMany(nilai::class, 'siswa_id');
    }

    public function siswakelas()
    {
    return $this->hasMany(siswakelas::class, 'siswa_id');
    }

}




<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Matpel extends Model
{
    use HasFactory;

    protected $table = "matpel";
    protected $fillable = ["nama_matpel"];


public function nilai()
{
    return $this->hasMany(nilai::class, 'matpel_id');
}

public function matpelkelas()
{
    return $this->hasMany(matpelkelas::class, 'matpel_id');
}


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiswaKelas extends Model
{
    protected $table = "siswa_kelas";
    protected $fillable = ['tahun_ajaran','siswa_id','kelas_id'];
}

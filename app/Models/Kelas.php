<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    protected $table = "kelas";
    protected $fillable = ["nama_kelas"];

    public function matpelkelas()
    {
    return $this->hasMany(matpelkelas::class, 'kelas_id');
    }

    public function siswakelas()
    {
    return $this->hasMany(siswakelas::class, 'kelas_id');
    }
}

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GuruController;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\MatpelController;
use App\Http\Controllers\NilaiController;
use App\Http\Controllers\KelasController;
use App\Models\Matpel;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.master');
});

Route::get('/guru', [GuruController::class, 'index']);
Route::get('/guru/create', [GuruController::class, 'create']);
Route::post('/guru', [GuruController::class, 'save']);
Route::get('/guru/{id}/edit', [GuruController::class, 'edit']);
Route::put('/guru/{id}', [GuruController::class, 'update']);
Route::delete('/guru/{id}', [GuruController::class, 'destroy']);

Route::get('/kelas', [KelasController::class, 'index']);
Route::get('/kelas/create', [KelasController::class, 'create']);
Route::post('/kelas', [KelasController::class, 'save']);
Route::get('/kelas/{id}/edit', [KelasController::class, 'edit']);
Route::put('/kelas/{id}', [KelasController::class, 'update']);
Route::delete('/kelas/{id}', [KelasController::class, 'destroy']);

Route::get('/matpel', [MatpelController::class, 'index']);
Route::get('/matpel/create', [MatpelController::class, 'create']);
Route::post('/matpel', [MatpelController::class, 'save']);
Route::get('/matpel/{id}/edit', [MatpelController::class, 'edit']);
Route::put('/matpel/{id}', [MatpelController::class, 'update']);
Route::delete('/matpel/{id}', [MatpelController::class, 'destroy']);





